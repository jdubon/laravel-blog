<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Operations\Post\CreateOperation;
use App\Operations\Post\DeleteOperation;
use App\Operations\Post\IndexOperation;
use App\Operations\Post\ShowOperation;
use App\Operations\Post\UpdateOperation;

class PostController extends Controller
{
    /**
     * Get collection data. It can be requested paginated
     *
     * @param IndexOperation $operation
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(IndexOperation $operation)
    {
        return $operation->perform();
    }

    /**
     * Shows an Item
     *
     * @param ShowOperation $operation
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowOperation $operation, $id)
    {
        return $operation->perform($id);
    }

    /**
     * Created a new Item
     *
     * @param CreateOperation $operation
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateOperation $operation)
    {
        return $operation->perform();
    }

    /**
     * Updates an Item
     *
     * @param UpdateOperation $operation
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateOperation $operation, $id)
    {
        return $operation->perform($id);
    }

    /**
     * Deletes an Item
     *
     * @param DeleteOperation $operation
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(DeleteOperation $operation, $id)
    {
        return $operation->perform($id);
    }
}
