<?php

namespace App\Operations;

use App\Repositories\RepositoryInterface;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class BaseOperation
{
    use ApiResponser;

    /**
     * Contains all the params from the request
     *
     * @var array
     */
    protected $params;

    /**
     * Operation Repository
     *
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(Request $request, RepositoryInterface $repository) 
    {
        $this->repository = $repository;
        $this->params = $request->all();
    }
}
