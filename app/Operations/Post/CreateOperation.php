<?php

namespace App\Operations\Post;

use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Resources\PostResource;
use App\Operations\BaseOperation;
use App\Operations\OperationInterface;
use App\Repositories\Post\PostRepository;

class CreateOperation extends BaseOperation implements OperationInterface
{
    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(CreatePostRequest $request, PostRepository $repository)
    {
        parent::__construct($request, $repository);
    }

    /**
     * Main entry point of Operation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perform(...$args)
    {
        return $this->create();
    }

    /**
     * Retriving the collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create()
    {
        $item = $this->repository->create($this->params);

        $this->data = new PostResource($item);
        return $this->successResponse();
    }
}
