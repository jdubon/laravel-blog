<?php

namespace App\Operations\Post;

use App\Operations\BaseOperation;
use App\Operations\OperationInterface;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class DeleteOperation extends BaseOperation implements OperationInterface
{
    /**
     * Item ID
     *
     * @var int
     */
    protected $id;

    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(Request $request, PostRepository $repository)
    {
        parent::__construct($request, $repository);
    }

    /**
     * Main entry point of Operation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perform(...$args)
    {
        $this->getId($args);

        return $this->update();
    }

    /**
     * Retriving the collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update()
    {
        $this->repository->delete($this->id);

        $this->data = [
            'message' => 'object was deleted',
        ];

        return $this->successResponse();
    }

    protected function getId($args)
    {
        if(isset($args[0]))
        {
            $this->id = $args[0];
        }
        else
        {
            throw new \Exception("You must specify an ID", 1);
        }
    }
}
