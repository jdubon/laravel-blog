<?php

namespace App\Operations\Post;

use App\Http\Resources\PostResource;
use App\Operations\BaseOperation;
use App\Operations\OperationInterface;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class IndexOperation extends BaseOperation implements OperationInterface
{
    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(Request $request, PostRepository $repository) 
    {
        parent::__construct($request, $repository);
    }

    /**
     * Main entry point of Operation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perform(...$args)
    {
        return $this->get();
    }

    /**
     * Retriving the collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function get()
    {
        $index = $this->repository->get();

        $this->data = PostResource::collection($index['collection']);
        $this->pagination = $index['pagination'];

        return $this->successResponse();
    }
}
