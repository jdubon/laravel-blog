<?php

namespace App\Operations\Post;

use App\Http\Resources\PostResource;
use App\Operations\BaseOperation;
use App\Operations\OperationInterface;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class ShowOperation extends BaseOperation implements OperationInterface
{
    /**
     * Item ID
     *
     * @var int
     */
    protected $id;

    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(Request $request, PostRepository $repository)
    {
        parent::__construct($request, $repository);
    }

    /**
     * Main entry point of Operation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perform(...$args)
    {
        $this->getId($args);

        return $this->show();
    }

    /**
     * Retriving the collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show()
    {
        $item = $this->repository->find($this->id);

        $this->data = new PostResource($item);
        return $this->successResponse();
    }

    protected function getId($args)
    {
        if(isset($args[0]))
        {
            $this->id = $args[0];
        }
        else
        {
            throw new \Exception("You must specify an ID", 1);
        }
    }
}
