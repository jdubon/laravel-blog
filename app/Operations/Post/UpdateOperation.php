<?php

namespace App\Operations\Post;

use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Operations\BaseOperation;
use App\Operations\OperationInterface;
use App\Repositories\Post\PostRepository;

class UpdateOperation extends BaseOperation implements OperationInterface
{
    /**
     * Item ID
     *
     * @var int
     */
    protected $id;

    /**
     * Setting up the operation
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(UpdatePostRequest $request, PostRepository $repository)
    {
        parent::__construct($request, $repository);
    }

    /**
     * Main entry point of Operation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perform(...$args)
    {
        $this->getId($args);

        return $this->update();
    }

    /**
     * Retriving the collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update()
    {
        $item = $this->repository->update($this->params, $this->id);

        $this->data = new PostResource($item);
        return $this->successResponse();
    }

    protected function getId($args)
    {
        if(isset($args[0]))
        {
            $this->id = $args[0];
        }
        else
        {
            throw new \Exception("You must specify an ID", 1);
        }
    }
}
