<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Traits\IndexCollection;

class PostRepository implements PostRepositoryInterface
{
    use IndexCollection;

    /**
     * The repository Model
     *
     * @var Model
     */
    protected $model;

    /**
     * Setting up the model
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * Index a Collection
     */
    public function get()
    {
        return $this->indexCollection($this->model);
    }

    /**
     * Shows an Item
     *
     * @param mixed $id
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Creates a new Item
     *
     * @param array $params
     */
    public function create(array $params)
    {
        return $this->model->create($params);
    }

    /**
     * Updates an Item
     *
     * @param array $params
     * @param mixed $id
     */
    public function update(array $params, $id)
    {
        $item = $this->model->findOrFail($id);
        $item->update($params);

        return $item->refresh();
    }

    /**
     * Deletes an Item
     *
     * @param mixed $id
     */
    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        return $item->delete();
    }
}
