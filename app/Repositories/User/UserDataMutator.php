<?php

namespace App\Repositories\User;

class UserDataMutator
{
    static function hashPassword($params)
    {
        if(isset($params['password']))
        {
            $params['password'] = bcrypt($params['password']);
        }

        return $params;
    }
}
