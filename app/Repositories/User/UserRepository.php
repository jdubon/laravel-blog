<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Models\User;
use App\Repositories\User\UserDataMutator;
use App\Traits\IndexCollection;

class UserRepository implements UserRepositoryInterface
{
    use IndexCollection;

    /**
     * The repository Model
     *
     * @var Model
     */
    protected $model;

    /**
     * Setting up the model
     *
     * @param Post $post
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Index a Collection
     */
    public function get()
    {
        return $this->indexCollection($this->model);
    }

    /**
     * Shows an Item
     *
     * @param mixed $id
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Creates a new Item
     *
     * @param array $params
     */
    public function create(array $params)
    {
        $params = UserDataMutator::hashPassword($params);
        return $this->model->create($params);
    }

    /**
     * Updates an Item
     *
     * @param array $params
     * @param mixed $id
     */
    public function update(array $params, $id)
    {
        $item = $this->model->findOrFail($id);

        $params = UserDataMutator::hashPassword($params);
        $item->update($params);

        return $item->refresh();
    }

    /**
     * Deletes an Item
     *
     * @param mixed $id
     */
    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        return $item->delete();
    }
}
