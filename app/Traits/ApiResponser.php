<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    protected $data = [];
    protected $pagination = [];

    /**
     * Build a success response
     *
     * @param array $data
     * @param int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function successResponse($code = Response::HTTP_OK)
    {
        $data = [];
        $data['data'] = $this->data;

        if(!empty($this->pagination)) {
            $data['pagination'] = $this->pagination;
        }

        return response()->json($data, $code);
    }

    /**
     * Build an error response
     *
     * @param string $message
     * @param int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function errorResponse($message, $code) 
    {
        return response()->json([
            'message' => $message,
            'code'  => $code
        ], $code);
    }
}