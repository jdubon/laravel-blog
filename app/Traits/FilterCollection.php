<?php

namespace App\Traits;

trait FilterCollection
{
    /**
     * Filter that will be applied to the collection listing
     *
     * @var array
     */
    protected $filters  = [];

    /**
     * Strict Filters this filter by AND insted of OR
     *
     * @var array
     */
    protected $sFilters = [];

    /**
     * Indicates if there are Filters or Strict Filters present
     *
     * @var boolean
     */
    protected $filtered = false;

    /**
     * Applies filters to the given model and return it filtered
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param bool $alreadyFiltered
     * @param bool $custom
     * @return Illuminate\Database\Eloquent\Model
     */
    protected function filterCollection($model, $alreadyFiltered = false, $custom = false)
    {
        if(!isset($this->filterable)) return $model;

        if(is_string($model))
            $model = new $model();

        $this->getRequestFilters();
        $this->filters = $this->cleanFilters($this->filters);
        $this->sFilters = $this->cleanFilters($this->sFilters);

        if(!$custom)
        {
            $sfilters = $this->sfilters($model, $alreadyFiltered);
            return $this->simpleFilter($sfilters[0], $sfilters[1]);
        }
        else
        {
            return $this->customFilters($model, $alreadyFiltered);
        }
    }

    /**
     * Strinct Filters, this will use AND instead of OR and = instead of like
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param bool $alreadyFiltered
     * @return void
     */
    protected function sFilters($model, $alreadyFiltered)
    {
        if(empty($this->sFilters)) return [$model, $alreadyFiltered];

        $filters = $this->sFilters;

        foreach ($filters as $key => $value)
        {
            $model = $model->where($key, '=', $value);
        }

        return [$model, TRUE];
    }

    /**
     * Get the request filters params
     *
     * @return void
     */
    protected function getRequestFilters()
    {
        $this->filtered = isset($this->params['filter']);

        if(!$this->filtered)
            $this->filtered = isset($this->params['sfilter']);

        if(isset($this->params['filter']))
            $this->filters = $this->params['filter'];

        if(isset($this->params['sfilter']))
            $this->sFilters = $this->params['sfilter'];
    }

    /**
     * This is to remove the not allowed filters
     *
     * @return void
     */
    protected function cleanFilters($filters)
    {
        if(empty($filters)) return;

        $currentFilters = $filters;
        $validFilters = [];
        foreach ($currentFilters as $key => $value)
        {
            if(in_array($key, $this->filterable))
            {
                $validFilters[$key] = $value;
            }
        }

        return $validFilters;
    }

    /**
     * Filter the given model with the fileds found as valid filters
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param boolean $alreadyFiltered
     * @return Illuminate\Database\Eloquent\Model
     */
    protected function simpleFilter($model, $alreadyFiltered)
    {
        if(!$this->filtered) return $model;

        $filters = $this->filters;
        if(!empty($filters))
        {
            if(!$alreadyFiltered)
            {
                foreach ($filters as $key => $value)
                {
                    $model = $model->orWhere($key, 'like', '%' . $value . '%');
                }
            }
            else
            {
                $model->where(function($query) use($filters) {
                    foreach ($filters as $key => $value)
                    {
                        $query = $query->orWhere($key, 'like', '%' . $value . '%');
                    }
                });
            }
        }

        return $model;
    }
}