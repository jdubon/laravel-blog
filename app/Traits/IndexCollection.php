<?php

namespace App\Traits;

trait IndexCollection
{
    protected $pagination = [];
    protected $page;
    protected $perPage = 25;

    /**
     * If paginated was sent it will build the paginated collection metadata,
     * otherwise it will return a simple collection
     *
     * @param Model $model
     * @return array
     */
    protected function indexCollection($model)
    {
        $this->page = request('page', false);
        $this->perPage = request('per_page', $this->perPage);

        if(is_string($model))
            $model = new $model();

        $collection = [];
        if($this->page)
        {
            $query      = $model->paginate($this->perPage);
            $collection = $query->items();
            $this->setPagination($query);
        }
        else
        {
            $collection = $model->get();
        }

        return [
            'collection' => $collection,
            'pagination' => $this->pagination,
        ];
    }

    /**
     * Set pagination metadata for the response
     *
     * @param Illuminate\Database\Eloquent\Model $resource
     * @return void
     */
    protected function setPagination($resource)
    {
        $this->pagination = [
            'per_page'     => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'last_page'    => $resource->lastPage(),
            'total'        => $resource->total(),
        ];
    }
}